from __future__ import annotations
import os
from math import ceil
from typing import Dict, Tuple
import os

# Don't add ingredients to recipes that isn't in ingredient_conversions.txt!

# When adding prices to ingredient_prices.tct, when ingredients quantity is
# specified in dl, msk or tsk, the ingredient must exist in  ingredient_conversions.txt

class Recipe():
    """
    Example use:

    recipe_x = Recipe("recipe_name")
    recipe_x = recipe_x.convert_recipe_to_grams()
    recipe_X = recipe_x.scale_recipe_size(200)
    recipe_x.save_recipe()

    """
    global ingredient_conversions
    global ingredient_prices

    def __init__(self, recipe_name):
        if recipe_name != "#CLONE#":
            self.name, self.batch_size, self.ingredients = self.load_recipe(recipe_name)

    @classmethod
    def load_ingredient_prices(cls):
        """
        Loads ingredient prices from text file.
        converts to "total weight in grams", "total cost"
        """
        with open("./ingredient_prices.txt") as f:
            for line in f:
                (ingredient, amount, unit, price) = line.split()
                if unit == 'g':
                    conversion_rate = 1
                if unit == 'kg':
                    conversion_rate = 1000
                if unit == 'dl':
                    conversion_rate = ingredient_conversions[ingredient][0]
                if unit == 'msk':
                    conversion_rate = ingredient_conversions[ingredient][1]
                if unit == 'tsk':
                    conversion_rate = ingredient_conversions[ingredient][2]

                # gram per sek
                ingredient_prices[ingredient] = float(amount) * conversion_rate, float(price)

    @classmethod
    def load_ingredient_conversions(cls):
        """
        Loads ingredient conversion rates. X to grams.
        Calculates how many grams each ingredient weights in dl,msk, tsk.
        """
        with open("./ingredient_conversions.txt") as f:
            for line in f:
                (ingredient, grams, amount, unit) = line.split()
                if unit == 'dl':
                    ingredient_conversions[ingredient] = float(grams) / float(amount), float(grams) / (
                                float(amount) * 6.6), float(grams) / (float(amount) * 20)
                if unit == 'msk':
                    ingredient_conversions[ingredient] = float(grams) / (float(amount) / 6.6), float(grams) / (
                        float(amount)), float(grams) / (float(amount) * 3)
                if unit == 'tsk':
                    ingredient_conversions[ingredient] = float(grams) / (float(amount) / 20), float(grams) / (
                                float(amount) / 3), float(grams) / (float(amount))

    @classmethod
    def list_all_recipes(cls) -> list(str):
        """
        Returns all available recipes
        """
        pass



    @classmethod
    def add_ingredient(cls):
        """
        Adds ingredient to ingredient_conversions.txt
        """
        pass

    @classmethod
    def list_all_ingredients(cls) -> list(str):
        """
        Returns all available ingredients
        """
        pass


    def clone(self) -> Recipe:
        clone = Recipe("#CLONE#")
        clone.name = self.name
        clone.batch_size = self.batch_size
        clone.ingredients = self.ingredients
        return clone

    def load_recipe(self, recipe_name) -> Tuple[str, int, dict[str, dict[str, int]]]:
        """
        Loads recipe from text file.
        Recipe example:
            Recipe_name batch_size
            ingredient_1 amount unit
            ...
            ingredient_n amount unit
        """
        ingredients = {}
        # check if the recipe exists
        recipePath = f"./recipes/{recipe_name}.txt"
        if not os.path.isfile(recipePath): #if the file/recipe doesn't exist, cast an error
            raise Exception("Could not load recipe. Given Recipe not found in recipes folder")
            exit(-2)

        # if recipe exits, parse and load data into memory
        with open(recipePath) as f:
            (name, batch_size) = f.readline().split()
            for line in f:
                (ingredient, amount, unit) = line.split()
                ingredients[ingredient] = dict()
                ingredients[ingredient]['amount'] = float(amount)
                ingredients[ingredient]['unit'] = unit
                print(f"{ingredient}: {ingredients[ingredient]['amount']} {ingredients[ingredient]['unit']}")
        return name, int(batch_size), ingredients

    def convert_recipe_to_grams(self) -> Recipe:
        """
        Returns recipe with all ingredients converted to grams.
        """
        converted_ingredients = {}
        for ingredient in self.ingredients:
            conversion_rate = int(0)
            ingredient_unit = self.ingredients[ingredient]['unit']

            if ingredient_unit == 'g':
                conversion_rate = 1
            elif ingredient_unit == 'kg':
                conversion_rate = 1000
            elif ingredient_unit == 'dl':
                conversion_rate = ingredient_conversions[ingredient][0]
            elif ingredient_unit == 'msk':
                conversion_rate = ingredient_conversions[ingredient][1]
            elif ingredient_unit == 'tsk':
                conversion_rate = ingredient_conversions[ingredient][2]

            grams = self.ingredients[ingredient]['amount'] * conversion_rate
            converted_ingredients[ingredient] = dict()
            converted_ingredients[ingredient]['amount'] = grams
            converted_ingredients[ingredient]['unit'] = 'g'

        gram_recipe = self.clone()
        gram_recipe.ingredients = converted_ingredients
        return gram_recipe

    def scale_recipe_to_single(self) -> Recipe:
        """
        Returns a version of the recipe scaled to batch size of one.
        """
        scaled_recipe = self.clone()
        scaled_recipe.batch_size = 1

        for ingredient in self.ingredients:
            scaled_recipe.ingredients[ingredient]['amount'] = self.ingredients[ingredient]['amount'] / self.batch_size

        return scaled_recipe

    def scale_recipe_size(self, target_batch_size) -> Recipe:
        """
        Returns a version of the recipe scaled to the given batch size.
        """
        single_recipe = self.scale_recipe_to_single()
        scaled_recipe = self.clone()
        scaled_recipe.batch_size = target_batch_size

        for ingredient in self.ingredients:
            scaled_recipe.ingredients[ingredient]['amount'] = single_recipe.ingredients[ingredient][
                                                                  'amount'] * target_batch_size

        return scaled_recipe

    def calculate_recipe_cost(self, exact):
        total_cost = 0
        for ingredient in self.ingredients: #change calculate_recipe_ingredient_packages and use in loop bellow
            ingredient_packages = self.ingredients[ingredient]['amount'] / ingredient_prices[ingredient][
                0] if exact == True else ceil(self.ingredients[ingredient]['amount'] / ingredient_prices[ingredient][0])
            total_cost += ingredient_packages * ingredient_prices[ingredient][1]
        return round(total_cost, 2)

    def calculate_recipe_cost_exact(self) -> float:
        """
        Returns what it would cost to just the necessary amount of ingredient packages needed for the recipe.
        """
        return self.calculate_recipe_cost(exact=True)

    def calculate_recipe_cost_whole_products(self) -> float:
        """
        Returns what it would cost to buy all ingredient packages needed for the recipe.
        """
        return self.calculate_recipe_cost(exact=False)

    def calculate_recipe_ingredient_packages(self) -> dict[str, int]:
        """
        Returns a dictionary of how many packages of ingredients is needed for the recipe.
        """
        ingredient_packages = {}
        for ingredient in self.ingredients:
            ingredient_packages[ingredient] = ceil(
                self.ingredients[ingredient]['amount'] / ingredient_prices[ingredient][0])

        return ingredient_packages

    def get_shopping_list(self) -> str:
        """
        Returns a string with how many packages of ingredients is needed for the recipe, with package price.
        """
        string = f"{'-' * 70}\nInköpslista:\n"
        ingredient_packages = self.calculate_recipe_ingredient_packages()
        for ingredient in ingredient_packages:
            string += f"{ingredient:<20} |{round(ingredient_packages[ingredient], 1):>4} st   | {int(ingredient_prices[ingredient][0]):>6} gram {ingredient_prices[ingredient][1]:>6} kr/st\n"
        return string

    def __str__(self) -> str:
        string = f"{'=' * 70}\n{self.name} {self.batch_size} st\n{'-' * 70}\nIngredienser:\n"
        for ingredient in self.ingredients:
            string += f"{ingredient:<20} {int(round(self.ingredients[ingredient]['amount'], 0)) if self.ingredients[ingredient]['unit'] == 'g' else round(self.ingredients[ingredient]['amount'], 1):>6} {self.ingredients[ingredient]['unit']}\n"
        string += self.convert_recipe_to_grams().get_shopping_list()
        string += f"{'-' * 70}\nTotal kostnad: {self.convert_recipe_to_grams().calculate_recipe_cost_exact()} kr\n"
        string += f"Total kostnad (hela produkter): {self.convert_recipe_to_grams().calculate_recipe_cost_whole_products()} kr\n"
        return string

    def save_recipe(self) -> None:
        """
        Saves current recipe to a text file as "RECIPE_NAME BATCH_SIZE st.txt".
        """
        # if the path to the file doesn't exist, create the directory
        savePath = "./recipes/converted"

        filename = f"{self.name} {self.batch_size} st"
        with open(f"{savePath}/{filename}.txt", "w") as text_file:
            text_file.write(self.__str__())


# Initialize
ingredient_conversions = {}
ingredient_prices = {}
Recipe.load_ingredient_conversions()
Recipe.load_ingredient_prices()

# Create recipe directories.
# Change "pathRecipe" if you want to change the location where files will be stored
pathRecipe = "./recipes"
pathConverted = f"{pathRecipe}/pathRecipe" # Path to converted recipes
# If the directories doesn't exist, create them and exit the program
if not os.path.isdir(pathRecipe):
    os.mkdir(pathRecipe)
    pathRecipe += "/converted"
    os.mkdir(pathConverted)
    exit(0)

# Single recipe, saved as "Hallongrottor 200 st.txt"
# hallongrottor = Recipe("hallongrottor")
# hallongrottor = hallongrottor.convert_recipe_to_grams()
# hallongrottor = hallongrottor.scale_recipe_size(500)
# hallongrottor.save_recipe()
# pass

# TODO:
# Make a function to create a recipe (Will be used by gui)
# Make a function to list all recipes (make a folder to hold all recipes, converted recipes can go to subfolder of that, ./recipes/convertions)
# Make function that returns information in a format that can be used by gui (not a simple string)
